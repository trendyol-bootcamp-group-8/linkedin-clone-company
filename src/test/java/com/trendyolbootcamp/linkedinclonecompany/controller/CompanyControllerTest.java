package com.trendyolbootcamp.linkedinclonecompany.controller;


import com.trendyolbootcamp.linkedinclonecompany.domain.Company;
import com.trendyolbootcamp.linkedinclonecompany.service.CompanyService;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;


public class CompanyControllerTest {

    private final String companyId = "cd035a98-76ba-4ebb-840c-4f641dacc87c";
    private final String companyName = "Test";

    @Autowired
    private MockMvc mvc;

    @Mock
    private CompanyService companyService;


    @InjectMocks
    private CompanyController companyController;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        companyController = new CompanyController(companyService);

    }

    @Test
    public void should_init_controller() {

        String result = companyController.checkController();
        assertEquals(result, "Hello from company controller!");
    }

    @Test
    public void getCompanyWithId_should_return_SC_OK_and_company() {

        //Arrange
        Company game = new Company();
        game.setId(companyId);
        doReturn(game).when(companyService).getCompanyWithId(companyId);

        //Act
        ResponseEntity<Company> result = companyController.getCompanyWithId(companyId);

        //Verify
        assertThat(result).isNotNull();
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.SC_OK);
        assertThat(Objects.requireNonNull(result.getBody()).getId()).isEqualTo(companyId);
    }


    @Test
    public void deleteCompany_should_return_SC_OK() {
        //Arrange
        doNothing().when(companyService).deleteCompany("123");
        //Act
        ResponseEntity result = companyController.deleteCompany("123");
        //Verify
        assertThat(result.getStatusCodeValue()).isEqualTo(HttpStatus.SC_OK);
    }








}