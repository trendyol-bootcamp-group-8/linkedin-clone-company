package com.trendyolbootcamp.linkedinclonecompany.service;

import com.trendyolbootcamp.linkedinclonecompany.domain.Company;
import com.trendyolbootcamp.linkedinclonecompany.exception.CompanyNotFoundException;
import com.trendyolbootcamp.linkedinclonecompany.repository.CompanyRepository;
import org.assertj.core.api.Assert;
import org.assertj.core.util.Lists;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.MockitoAnnotations.initMocks;


@RunWith(MockitoJUnitRunner.class)
class CompanyServiceTest {

    @InjectMocks
    CompanyService companyService;
    Company company;

    @Mock
    CompanyRepository companyRepository;



    @BeforeEach
    private void beforeEach() {
        initMocks(this);

    }



    @Test
    public void it_should_insert_company_when_call_insertCompany()throws Exception{

        Company c = new Company("Test");
        Mockito.when(companyRepository.insert(c)).thenReturn(c);

        Company result1 = companyService.insertCompany(c);

        assertEquals("Test", result1.getName());

    }


    @Test
    public void it_should_delete_company_when_call_deleteCompany(){
        Company c = new Company("Test");
        Mockito.when(companyRepository.insert(c)).thenReturn(c);

        companyService.deleteCompany(c.getId());

        assertEquals(true, companyService.findAll().isEmpty());
    }

    @Test
    public void getCompanyWithId_should_throw_IllegalArgumentException_when_id_null() {

        String id = null;

        Throwable throwable = catchThrowable(() -> companyService.getCompanyWithId(id));

        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }


    @Test
    public void it_should_update_company_when_call_updateCompany(){
        String newName = "lele";
        Company c = new Company("Test");
       /* Mockito.when(companyRepository.findByIdOptional(c.getId())).thenReturn(Optional.of(c));

        companyService.updateCompany(c.getId(),  Optional.empty(),  Optional.of(newName),  Optional.empty(),  Optional.empty(),  Optional.empty(), null);

        assertEquals(c.getId(), companyService.getCompanyWithId(c.getId()));*/

    }

    @Test
    public void it_should_throw_companyNotFoundException_when_there_is_no_company(){
     
        // Arrange
        Company c = new Company("Test");
        Mockito.when(companyRepository.insert(c)).thenReturn(c);

        // Act
        companyService.deleteCompany(c.getId());

        // Assert
        assertThrows(CompanyNotFoundException.class, () -> companyService.getCompanyWithId(c.getId()));

    }



    @Test
    public void it_should_get_company_when_call_getCompanyWithId(){

        Company c = new Company("Test");
        Mockito.when(companyRepository.findByIdOptional(c.getId())).thenReturn(Optional.of(c));

        Company result1 = companyService.getCompanyWithId(c.getId());

        assertEquals("Test", result1.getName());
    }

    @Test
    public void it_should_return_all_company_when_call_findAll(){
    }


    @Test
    public void it_should_post_a_job_when_call_postAJob(){
    }



    @Test
    public void it_should_return_all_job_postings_when_call_listAllJobPostings(){

    }


    @Test
    public void it_should_return_all_applicants_when_call_getAllApplicants(){
    }


    @Test
    public void it_should_return_candidate_when_call_getCandidateByID(){


    }

}